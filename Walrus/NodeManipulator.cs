﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace Walrus
{
    class NodeManipulator
    {
        private Node mNode = null;
        private Model3DGroup mEdgesContainer;
        
        #region SINGLETON
        private static NodeManipulator sInstance = null;
        private NodeManipulator()
        {

        }
        public static NodeManipulator Instance()
        {
            if (sInstance == null)
                sInstance = new NodeManipulator();
            return sInstance;
        }
        #endregion

        /// <summary>
        /// Edges container for changing 
        /// old edges to new ones
        /// </summary>
        public void setEdgesContainer(Model3DGroup edgesContainer)
        {
            mEdgesContainer = edgesContainer;
        }

        /// <summary>
        /// Set node for manipulation
        /// </summary>
        public void SetNode(Node n)
        {
            mNode = n;
        }

        /// <summary>
        /// Stop changing node position. Free node and
        /// unfreeze camera
        /// </summary>
        public void EndManipulation()
        {
            if (mNode == null)
                return;

            mNode.MouseRightUnclick();
            mNode = null;
        }

        /// <summary>
        /// Rotate or change nodes cone length and its children positions
        /// </summary>
        /// <param name="deltaAngle">Rotation change</param>
        /// <param name="deltaShift">Cone lenght change</param>
        public void move(double deltaAngle, double deltaShift)
        {
            //dont move root, leafs or nodes with wrong data
            if (mNode == null)
                return;
            if (mNode.Parent == null)
                return;
            if (mNode.Dir == null || mNode.Cross == null || mNode.Cross.Length==0)
                return;
            if (mNode.isLeaf())
                return;
            
            //count movement
            double angle = deltaAngle / 80.0;
            double shift = deltaShift / 1000.0;

            mNode.ConeShift += shift;
            if (mNode.ConeShift < 0.0001)
                mNode.ConeShift = 0.0001;
            
            Rotation3D rotation = new AxisAngleRotation3D(mNode.Dir, angle * (180 / Math.PI));
            RotateTransform3D rotateTranform = new RotateTransform3D(rotation, mNode.Parent.Position);
            mNode.Cross = rotateTranform.Transform(mNode.Cross);
            
            //update positions of this node and its children
            GraphRenderer renderer = new GraphRenderer();
            renderer.renderNode(mNode, mNode.Dir, mNode.Cross);

            //update nodes data
            updateTransformation(mNode);
            //create new geometry
            createNewEdges(mNode.Parent);
        }

        /// <summary>
        /// Extend nodes cone length
        /// </summary>
        public void extend(double delta)
        {
            //dont extend leafs, root or wrong nodes
            if (mNode == null)
                return;
            if (mNode.Parent == null)
                return;
            if (mNode.Dir == null || mNode.Cross == null || mNode.Cross.Length == 0)
                return;
            if (mNode.isLeaf())
                return;

            mNode.ConeLength += delta / 5000.0;

            GraphRenderer renderer = new GraphRenderer();
            renderer.renderNode(mNode, mNode.Dir, mNode.Cross);
            updateTransformation(mNode);
            createNewEdges(mNode.Parent);
        }

        /// <summary>
        /// Update node and its children geometry transformation.
        /// </summary>
        private void updateTransformation(Node n)
        {
            n.UpdateTransformation();
            foreach(var c in n.outEdges)
            {
                updateTransformation(c);
            }
        }

        /// <summary>
        /// Creates new edges from new positon
        /// </summary>
        private void createNewEdges(Node n)
        {
            //remove previous edges geometry
            deleteEdges(n);

            //create additional out edges owned by this node
            foreach (Node additional in n.additionalOutEdges)
            {
                GeometryModel3D addEdge = GeometryBuilder.BuildLine(
                   n.Position, additional.Position,
                   new DiffuseMaterial(new SolidColorBrush(Colors.Purple)));

                n.AddAdditionalEdgeGeometry(additional.Id, addEdge);
            }
            //create additional in edges NOT owned by this node
            foreach (Node additional in n.additionalInEdges)
            {
                GeometryModel3D addEdge = GeometryBuilder.BuildLine(
                   additional.Position, n.Position,
                   new DiffuseMaterial(new SolidColorBrush(Colors.Purple)));

                additional.AddAdditionalEdgeGeometry(n.Id, addEdge);
            }
            //create out edges owned by this node
            foreach (var child in n.outEdges)
            {
                var edge = GeometryBuilder.BuildLine(n.Position, child.Position,
                        new DiffuseMaterial(new SolidColorBrush(Colors.Purple)));

                n.outEdgesGeomtery.Children.Add(edge);

            }
            //add edges to geometry container
            addEdges(n);

            //continue to create edges recursively
            foreach (var c in n.outEdges)
            {
                createNewEdges(c);
            }
        }

        /// <summary>
        /// removing edges geometry from node
        /// </summary>
        /// <param name="n"></param>
        private void deleteEdges(Node n)
        {
            foreach (var edge in n.outEdgesGeomtery.Children)
            {
                mEdgesContainer.Children.Remove(edge);
            }
            foreach (var edge in n.additionalEdgesGeomtery)
            {
                mEdgesContainer.Children.Remove(edge.Value);
            }
            if (n.additionalInEdges.Count != 0)
            {
                foreach (var owner in n.additionalInEdges)
                {
                    mEdgesContainer.Children.Remove(owner.additionalEdgesGeomtery[n.Id]);
                    owner.additionalEdgesGeomtery.Remove(n.Id);
                }
            }
            n.outEdgesGeomtery = new Model3DGroup();
            n.additionalEdgesGeomtery.Clear();
        }

        /// <summary>
        /// adding edges to node
        /// </summary>
        /// <param name="n"></param>
        private void addEdges(Node n)
        {
            if (n.EdgesCollapsed == false)
            {
                //add normal out edges
                foreach (var edge in n.outEdgesGeomtery.Children)
                {
                    mEdgesContainer.Children.Add(edge);
                }
                //add additional out edges owned by this node
                foreach (var edge in n.additionalEdgesGeomtery)
                {
                    Node connectedNode = n.additionalOutEdges.Find(x => x.Id == edge.Key);
                    if (connectedNode.NodeCollapsed == false)
                        mEdgesContainer.Children.Add(edge.Value);
                }
                //add additional out edges NOT owned by this node
                if (n.additionalInEdges.Count != 0)
                {
                    foreach (var owner in n.additionalInEdges)
                    {
                        if (owner.NodeCollapsed == false)
                            mEdgesContainer.Children.Add(owner.additionalEdgesGeomtery[n.Id]);
                    }
                }
            }
        }
    }
}