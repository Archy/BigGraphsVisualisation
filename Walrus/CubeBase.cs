﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace Walrus
{
    /// <summary>
    ///  This class handles visualisation of cube -> graphical repsresntation of single graphs node
    /// </summary>
    class CubeBase
    {
        /// <summary>
        ///  Cubes edge width
        /// </summary>
        private const double a = 0.01;

        /// <summary>
        ///  Tetraedr edge width
        /// </summary>
        private const double aT = 0.06;

        private ModelUIElement3D mModel;
        private GeometryModel3D mGeometry;

        #region materials
        private MaterialGroup mFocusedMaterial;
        private MaterialGroup mNormalMaterial;
        private MaterialGroup mCollsapsedMaterial;
        #endregion

        private Node mNode;

        private bool mCollapseOwner = false;

        private Label mInfoLabel;
        private string mInfo;

        private bool mClicked = false;
        private bool mMouseLeft = false;

        /// <summary>
        ///  Info shown when cube is focused 
        /// </summary>
        public string Info
        {
            get
            {
                return mInfo;
            }

            set
            {
                mInfo = value;
            }
        }

        /// <summary>
        ///  3D representation of node 
        /// </summary>
        public ModelUIElement3D Model
        {
            get
            {
                return mModel;
            }

            set
            {
                mModel = value;
            }
        }

        /// <summary>
        ///  Cubes edge width
        /// </summary>
        public double A
        {
            get
            {
                return 2*a;
            }
        }

        /// <summary>
        /// Starts collapsing
        /// </summary>
        public bool CollapseOwner
        {
            get
            {
                return mCollapseOwner;
            }

            set
            {
                mCollapseOwner = value;
            }
        }

        /// <summary>
        /// Model geometry
        /// </summary>
        public GeometryModel3D Geometry
        {
            get
            {
                return mGeometry;
            }

            set
            {
                mGeometry = value;
            }
        }

        /// <summary>
        ///  Cube init 
        /// </summary>
        public CubeBase(Node owner, string info, Label infoLabel)
        {
            mNode = owner;

            //info displayed in the info label 
            mInfoLabel = infoLabel;
            mInfo = info; 

            //create material
            mNormalMaterial = new MaterialGroup();
            mNormalMaterial.Children.Add(new DiffuseMaterial(new SolidColorBrush(Colors.DarkOrange)));

            mFocusedMaterial = new MaterialGroup();
            mFocusedMaterial.Children.Add(new DiffuseMaterial(new SolidColorBrush(Colors.Blue)));

            mCollsapsedMaterial = new MaterialGroup();
            mCollsapsedMaterial.Children.Add(new DiffuseMaterial(new SolidColorBrush(Colors.Red)));

            //create cube model
            mModel = new ModelUIElement3D();
            //set mouse enter/leave action
            mModel.MouseEnter += Bar_MouseEnter;
            mModel.MouseLeave += Bar_MouseLeave;

            //for expanding
            mModel.MouseLeftButtonDown += MModel_MouseLeftButtonDown;

            //for manipulating nodes positions
            mModel.MouseRightButtonDown += owner.MouseRightClick;
        }

        /// <summary>
        ///  Create cube geometry
        /// </summary>
        public void CreateGeometry(TranslateTransform3D tranform, RotateTransform3D rotation = null)
        {
            mGeometry = GeometryBuilder.BuildCube(a, mNormalMaterial, tranform);
            mModel.Model = mGeometry;
            mModel.Focusable = true;
        }

        /// <summary>
        /// Render node as highlighted
        /// </summary>
        public void Focus()
        {
            mClicked = true;
        }

        /// <summary>
        /// Render node as unhighlighted
        /// </summary>
        public void Unfocus()
        {
            mClicked = false;

            if(mMouseLeft)
            {
                if (mCollapseOwner)
                {
                    mGeometry.Material = mCollsapsedMaterial;
                }
                else
                {
                    mGeometry.Material = mNormalMaterial;
                }
            }   
        }

        /// <summary>
        /// Changes node color to show 
        /// that it owns hidden children
        /// </summary>
        public void startCollapsing()
        {
            mCollapseOwner = true;
            mGeometry.Material = mCollsapsedMaterial;
        }

        /// <summary>
        /// Restores node color back to normal
        /// </summary>
        public void stopCollapsing()
        {
            mCollapseOwner = false;
            mGeometry.Material = mNormalMaterial;
        }

        /// <summary>
        /// Node highlighted
        /// </summary>
        private void Bar_MouseEnter(object sender, MouseEventArgs e)
        {
            //change color, set and show info

            //mModel.Model = mGeometry = mGeometry.Clone();
            mGeometry.Material = mFocusedMaterial;
            //mModel.Model = mGeometry;
            mInfoLabel.Content = mInfo;
            mInfoLabel.Visibility = Visibility.Visible;

            mMouseLeft = false;
        }

        /// <summary>
        /// Node unhighlighted
        /// </summary>
        private void Bar_MouseLeave(object sender, MouseEventArgs e)
        {
            mMouseLeft = true;

            if (mClicked == true)
                return;

            //change color back, hide info
            if(mCollapseOwner)
            {
                mGeometry.Material = mCollsapsedMaterial;
            }
            else
            {
                mGeometry.Material = mNormalMaterial;
            }
            mInfoLabel.Visibility = Visibility.Hidden;
            //mGeometry.Freeze();
        }

        /// <summary>
        /// Node clicked for (de)collapsing
        /// </summary>
        private void MModel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (mCollapseOwner == false)
            {
                //collapse child nodes
                NodeCollapser.Instance().CollapseNode(mNode);
            }
            else
            {
                //show child nodes
                NodeCollapser.Instance().Decollapse(mNode);
            }
        }
    }
}
