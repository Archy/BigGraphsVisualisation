﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Walrus
{
    class SpanningTreeSeeker
    {
        private Node root;
        private List<Node> nodes = new List<Node>();

        /// <summary>
        /// Create spanning tree seeker and remember root for further actions
        /// </summary>
        public SpanningTreeSeeker(Node r)
        {
            root = r;
            nodes.Add(root);
        }

        /// <summary>
        /// Find spanning tree recursively
        /// </summary>
        public void findSpanningTree(Node parent)
        {
            List<Node> toRemove = new List<Node>();
            foreach (Node n in parent.outEdges)
            {
                if (nodes.Contains(n))
                {
                    //edge is needless and can be removed
                    toRemove.Add(n);
                    n.RemoveInEdge(parent);
                    parent.AddAdditionalOutEdges(n);
                    n.AddAdditionalInEdges(parent);
                        
                }
                else
                {
                    //edge must be kept
                    n.setParent(parent);
                    nodes.Add(n);
                    findSpanningTree(n);
                }
            }
            parent.RemoveOutEdges(toRemove);
        }
    }
}
