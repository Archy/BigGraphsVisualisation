﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Walrus
{
    static class SphereDistributor
    {
        /// <summary>
        /// Creates sphere distribution points for n<8
        /// </summary>
        static SphereDistributor()
        {
            //n = 1
            mDist.Add (new List<Point>());
            mDist[0].Add(new Point(0, 0));

            //n = 2
            mDist.Add(new List<Point>());
            mDist[1].Add(new Point(0, 0));
            mDist[1].Add(new Point(Math.PI, 0));

            //n = 3
            mDist.Add(new List<Point>());
            mDist[2].Add(new Point(0, 0));
            mDist[2].Add(new Point(2.0*Math.PI/3.0, 0));
            mDist[2].Add(new Point(2.0 * Math.PI / 3.0, Math.PI));

            //n = 4
            mDist.Add(new List<Point>());
            mDist[3].Add(new Point(0, 0));
            mDist[3].Add(new Point(2.0 * Math.PI / 3.0, 0));
            mDist[3].Add(new Point(2.0 * Math.PI / 3.0, 2.0 *Math.PI / 3.0));
            mDist[3].Add(new Point(2.0 * Math.PI / 3.0, 4.0* Math.PI / 3.0));

            // n = 5
            mDist.Add(new List<Point>());
            mDist[4].Add(new Point(Math.PI / 3.0, 0));
            mDist[4].Add(new Point(Math.PI / 3.0, Math.PI));
            mDist[4].Add(new Point(2.0 * Math.PI / 3.0, 0));
            mDist[4].Add(new Point(2.0 * Math.PI / 3.0, 2.0 * Math.PI / 3.0));
            mDist[4].Add(new Point(2.0 * Math.PI / 3.0, 4.0 * Math.PI / 3.0));

            // n = 6
            mDist.Add(new List<Point>());
            mDist[5].Add(new Point(Math.PI / 4.0, 0));
            mDist[5].Add(new Point(Math.PI / 4.0, 2.0 * Math.PI / 3.0));
            mDist[5].Add(new Point(Math.PI / 4.0, 4.0 * Math.PI / 3.0));
            mDist[5].Add(new Point(Math.PI / 4.0 + 2.0 * Math.PI / 4.0, 0));
            mDist[5].Add(new Point(Math.PI / 4.0 + 2.0 * Math.PI / 4.0, 2.0 * Math.PI / 3.0));
            mDist[5].Add(new Point(Math.PI / 4.0 + 2.0 * Math.PI / 4.0, 4.0 * Math.PI / 3.0));

            // n = 7
            mDist.Add(new List<Point>());
            mDist[6].Add(new Point(Math.PI / 4.0, 0));
            mDist[6].Add(new Point(Math.PI / 4.0, 2.0 * Math.PI / 3.0));
            mDist[6].Add(new Point(Math.PI / 4.0, 4.0 * Math.PI / 3.0));
            
            mDist[6].Add(new Point(Math.PI / 4.0 + 2.0 * Math.PI / 4.0, 0));
            mDist[6].Add(new Point(Math.PI / 4.0 + 2.0 * Math.PI / 4.0, Math.PI / 2.0));
            mDist[6].Add(new Point(Math.PI / 4.0 + 2.0 * Math.PI / 4.0, Math.PI));
            mDist[6].Add(new Point(Math.PI / 4.0 + 2.0 * Math.PI / 4.0, 3.0 * Math.PI / 2.0));

            // n = 8
            mDist.Add(new List<Point>());
            mDist[7].Add(new Point(Math.PI / 4.0, 0));
            mDist[7].Add(new Point(Math.PI / 4.0, Math.PI / 2.0));
            mDist[7].Add(new Point(Math.PI / 4.0, Math.PI));
            mDist[7].Add(new Point(Math.PI / 4.0, 3.0 * Math.PI / 2.0));
       
            mDist[7].Add(new Point(Math.PI / 4.0 + 2.0 * Math.PI / 4.0, 0));
            mDist[7].Add(new Point(Math.PI / 4.0 + 2.0 * Math.PI / 4.0, Math.PI / 2.0));
            mDist[7].Add(new Point(Math.PI / 4.0 + 2.0 * Math.PI / 4.0, Math.PI));
            mDist[7].Add(new Point(Math.PI / 4.0 + 2.0 * Math.PI / 4.0, 3.0 * Math.PI / 2.0));

        }

        /// <summary>
        /// Return list of evenly distributed points latitudes and longitudes
        /// </summary>
        public static List<Point> getDistribution(int n)
        {
            if (n <= 0)
                return null;

            return mDist[n-1];
        }

        private static List<List<Point>> mDist = new List<List<Point>>();
    }
}
