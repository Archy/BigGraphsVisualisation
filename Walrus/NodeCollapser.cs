﻿using System.Windows.Media.Media3D;

namespace Walrus
{
    class NodeCollapser
    {
        private int mFilter = 0;
        ContainerUIElement3D mNodesContainer;
        Model3DGroup mEdgesContainer;

        #region singleton

        private static NodeCollapser mInstance = null;

        /// <summary>
        /// singleton
        /// </summary>
        public static NodeCollapser Instance()
        {
            if (mInstance == null)
                mInstance = new NodeCollapser();

            return mInstance;
        }

        private NodeCollapser()
        {

        }

        #endregion

        /// <summary>
        /// Remove all children nodes geometry of node 
        /// with given or smaller nr of children
        /// </summary>
        public void setData(ContainerUIElement3D nodesContainer,
            Model3DGroup edgesContainer)
        {
            mNodesContainer = nodesContainer;
            mEdgesContainer = edgesContainer;
        }

        /// <summary>
        /// Collapse nodes children
        /// </summary>
        public void CollapseNode(Node n)
        {
            if (n.Children == 0)
                return;

            //remove edges and start removing children geomtry
            n.EdgesCollapsed = true;
            deleteEdges(n);

            foreach (var c in n.outEdges)
            {
                collapseGeometry(c);
            }

            n.Geometry.startCollapsing();
        }

        /// <summary>
        /// Set nr of children to start collapsing
        /// </summary>
        public void setFilter(int n)
        {
            mFilter = n;
        }

        /// <summary>
        /// Show nodes again
        /// </summary>
        public void Decollapse(Node n)
        {
            n.Geometry.stopCollapsing();
            n.NodeCollapsed = false;

            n.EdgesCollapsed = false;

            addEdges(n);
            foreach (var c in n.outEdges)
            {
                decollapseDSF(c);
            }
        }

        /// <summary>
        /// Using DSF algorithm show nodes
        /// Stop expanding on collapse owners
        /// </summary>
        private void decollapseDSF(Node n)
        {
            mNodesContainer.Children.Add(n.Geometry.Model);
            n.NodeCollapsed = false;
            if (n.Geometry.CollapseOwner)
            {
                n.Geometry.startCollapsing();
                return;
            }

            n.EdgesCollapsed = false;
            addEdges(n);
            foreach (var c in n.outEdges)
            {
                decollapseDSF(c);
            }
        }

        /// <summary>
        /// Hide nodes starting from root
        /// </summary>
        public void Collapse(Node root)
        {
            foreach (Node c in root.outEdges)
            {
                findStartNode(c);
            }
        }

        /// <summary>
        /// Using DSF find nodes with given or smaller nr of children
        /// </summary>
        private void findStartNode(Node n)
        {
            //ensure this node geometry is not removed
            if (n.NodeCollapsed)
            {
                n.NodeCollapsed = false;
                mNodesContainer.Children.Add(n.Geometry.Model);
            }

            if (n.Children < mFilter && n.Children > 0)
            {
                //remove edges and start removing children geomtry
                n.EdgesCollapsed = true;
                deleteEdges(n);
                foreach (var c in n.outEdges)
                {
                    collapseGeometry(c);
                }

                n.Geometry.startCollapsing();

            }
            else
            {
                //ensure edges geometry is not removed
                n.Geometry.stopCollapsing();
                if (n.EdgesCollapsed)
                {
                    addEdges(n);
                }
                n.EdgesCollapsed = false;

                //keep looking
                foreach (var c in n.outEdges)
                {
                    findStartNode(c);
                }
            }
        }

        /// <summary>
        /// Removes edges and nodes geometry in order
        /// to hide it
        /// </summary>
        private void collapseGeometry(Node n)
        {
            n.NodeCollapsed = true;
            mNodesContainer.Children.Remove(n.Geometry.Model);

            n.EdgesCollapsed = true;
            deleteEdges(n);
            foreach (var c in n.outEdges)
            {
                collapseGeometry(c);
            }
        }

        /// <summary>
        /// removing edges from node
        /// </summary>
        /// <param name="n"></param>
        private void deleteEdges(Node n)
        {
            //delete out edges owned by this node
            foreach (var edge in n.outEdgesGeomtery.Children)
            {
                mEdgesContainer.Children.Remove(edge);
            }
            //delete additional out edges owned by this node
            foreach (var edge in n.additionalEdgesGeomtery)
            {
                mEdgesContainer.Children.Remove(edge.Value);
            }
            //delete additional out edges NOT owned by this node
            if (n.additionalInEdges.Count != 0)
            {
                foreach (var owner in n.additionalInEdges)
                {
                    mEdgesContainer.Children.Remove(owner.additionalEdgesGeomtery[n.Id]);
                }
            }
        }

        /// <summary>
        /// adding edges to node
        /// </summary>
        /// <param name="n"></param>
        private void addEdges(Node n)
        {
            //restore edges owned by this node
            foreach (var edge in n.outEdgesGeomtery.Children)
            {
                mEdgesContainer.Children.Add(edge);
            }
            //restore additional edges owned by this node
            foreach (var edge in n.additionalEdgesGeomtery)
            {
                Node connectedNode = n.additionalOutEdges.Find(x => x.Id == edge.Key);
                if (connectedNode.NodeCollapsed == false)
                    mEdgesContainer.Children.Add(edge.Value);
            }
            //restore additional edges NOT owned by this node
            if (n.additionalInEdges.Count != 0)
            {
                foreach (var owner in n.additionalInEdges)
                {
                    if(owner.NodeCollapsed == false)
                        mEdgesContainer.Children.Add(owner.additionalEdgesGeomtery[n.Id]);
                }
            }
        }
    }
}
