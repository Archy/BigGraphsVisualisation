﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Xml;

namespace Walrus
{
    class GraphParser
    {
        private Node mRoot = null;
        private string mRootId;
        private Dictionary<string, Node> mNodes = new Dictionary<string, Node>();
        XmlNode mGraph;

        bool mNodeInfo = false;
        string mInofID;
        string deaulfInfo;

        public int EdgesNr { get; private set; } = 0;
        public int NodesNr { get; private set; } = 0;

        /// <summary>
        /// Load graph from graphml file.
        /// Returns graphs root node
        /// </summary>
        public Node LoadGraph(string file, Label infoLabel)
        {
            //open file       
            XmlDocument doc = new XmlDocument();
            doc.Load(file);
            mGraph = doc.DocumentElement["graph"];

            mRootId = mGraph.Attributes["root"].InnerText;

            if (mRootId == null)
                throw new Exception("NO ROOT NODE!!!");

            CheckNodeInfo(doc.DocumentElement);
            ParseNodes(infoLabel);
            ParseEdges();
            return mRoot;
        }

        /// <summary>
        /// Load graph nodes from graphml file
        /// </summary>
        private void ParseNodes(Label infoLabel)
        {
            //load all nodes
            int intID = 0;

            foreach(XmlNode node in mGraph)
            {
                if(node.Name == "node")
                {
                    string id = node.Attributes["id"].InnerText;
                    if(!mNodeInfo || node.HasChildNodes==false)
                    {
                        mNodes[id] = new Node(id, deaulfInfo, infoLabel);
                    }
                    else
                    {
                        bool infoFound = false;

                        foreach (XmlNode data in node.ChildNodes)
                        {
                            if(data.Name=="data" && data.Attributes["key"].InnerText== mInofID)
                            {
                                infoFound = true;
                                mNodes[id] = new Node(id, data.InnerText, infoLabel);
                                break;
                            }
                        }

                        if(infoFound == false)
                        {
                            mNodes[id] = new Node(id, deaulfInfo, infoLabel);
                        }
                    }

                    if(mRootId == id)
                    {
                        mRoot = mNodes[id];
                    }
                    
                    ++intID;
                }
            }

            NodesNr = intID;
        }

        /// <summary>
        /// Load graph edges from graphml file
        /// and set them in nodes
        /// </summary>
        private void ParseEdges()
        {
            //load and set edges
            foreach (XmlNode node in mGraph)
            {
                if (node.Name == "edge")
                {
                    ++EdgesNr;

                    string from = node.Attributes["source"].InnerText;
                    string to = node.Attributes["target"].InnerText;

                    mNodes[from].AddOutEdges(mNodes[to]);
                    mNodes[to].AddInEdges(mNodes[from]);

                    //only for trees
                    mNodes[to].setParent(mNodes[from]);
                }
            }
        }

        /// <summary>
        /// Check if graphml file has info for nodes
        /// </summary>
        private void CheckNodeInfo(XmlNode root)
        {
            deaulfInfo = "NO INFO IN GRAPHML";

            foreach (XmlNode node in root)
            {
                if (node.Name == "key" && node.Attributes["for"].InnerText=="node" && node.Attributes["attr.name"].InnerText=="info")
                {
                    mNodeInfo = true;
                    mInofID = node.Attributes["id"].InnerText;
                    if(node["default"] != null)
                    {
                        deaulfInfo = node["default"].InnerText;
                    }
                    else
                    {
                        deaulfInfo = "NO INFO";
                    }

                    break;
                }
            }

            return;
        }
    }
}
