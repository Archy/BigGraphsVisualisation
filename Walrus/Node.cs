﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Media3D;

namespace Walrus
{
    class Node
    {
        private string mId;
        private CubeBase mGeometry;
        private List<Node> mOutEdges = new List<Node>();
        private List<Node> mInEdges = new List<Node>();
        private List<Node> mAdditionalInEdges = new List<Node>();
        private List<Node> mAdditionalOutEdges = new List<Node>();
        private Node mInEdge = null;

        private bool mLevelOwner = false;
        private int mLeafs = 0;
        private int mChildren = 0;

        private double mR;
        private double mConeLength = 0;
        private double mConeShift = 0;
        private double mConeAngle = 0;

        private Point3D mPosition = new Point3D(0, 0, 0);

        private Model3DGroup outEdgesGeometry = new Model3DGroup();
        private Dictionary<string, GeometryModel3D> additionalEdgesGeometry = new Dictionary<string, GeometryModel3D>();

        private bool edgesCollapsed = false;
        private bool nodeCollapsed = false;

        Vector3D mDir;
        Vector3D mCross;

        #region properties
        /// <summary>
        /// Node id
        /// </summary>
        public string Id
        {
            get
            {
                return mId;
            }
        }

        /// <summary>
        /// Node geometrical representation
        /// </summary>
        public CubeBase Geometry
        {
            get
            {
                return mGeometry;
            }
        }

        /// <summary>
        /// Returns list of nodes, that represent edges comming out of this one 
        /// </summary>
        public List<Node> outEdges
        {
            get
            {
                return mOutEdges;
            }
        }

        /// <summary>
        /// Returns list of nodes, that represent edges comming out of this one 
        /// </summary>
        public List<Node> additionalInEdges
        {
            get
            {
                return mAdditionalInEdges;
            }
        }

        public List<Node> additionalOutEdges
        {
            get
            {
                return mAdditionalOutEdges;
            }
        }

        /// <summary>
        /// Returns models group of edges geometry
        /// </summary>
        public Model3DGroup outEdgesGeomtery
        {
            get
            {
                return outEdgesGeometry;
            }

            set
            {
                outEdgesGeometry = value;
            }
        }

        /// <summary>
        /// Returns models group of edges geometry, which are not in spanning tree
        /// </summary>
        public Dictionary<string, GeometryModel3D> additionalEdgesGeomtery
        {
            get
            {
                return additionalEdgesGeometry;
            }

            set
            {
                additionalEdgesGeometry = value;
            }
        }

        /// <summary>
        /// Returns list of nodes, that represent edges going into of this one 
        /// </summary>
        public List<Node> inEdges
        {
            get
            {
                return mInEdges;
            }
        }

        /// <summary>
        /// Nodes parent. Null for root
        /// </summary>
        public Node Parent
        {
            set
            {
                mInEdge = value;
            }

            get
            {
                return mInEdge;
            }
        }

        /// <summary>
        /// is node a level owner
        /// </summary>
        public bool LevelOwner
        {
            get
            {
                return mLevelOwner;
            }
            set
            {
                mLevelOwner = value;
            }
        }

        /// <summary>
        /// number of nodes leafs
        /// </summary>
        public int Children
        {
            get
            {
                return mChildren;
            }
            set
            {
                mChildren = value;
            }
        }

        /// <summary>
        /// total number of nodes children
        /// </summary>
        public int Leafs
        {
            get
            {
                return mLeafs;
            }
            set
            {
                mLeafs = value;
            }
        }

        /// <summary>
        /// Node position in 3d space
        /// </summary>
        public Point3D Position
        {
            get
            {
                return mPosition;
            }
            set
            {
                mPosition = value;
            }
        }

        /// <summary>
        /// Radius of nodes group
        /// </summary>
        public double R
        {
            get
            {
                return mR;
            }

            set
            {
                mR = value;
            }
        }

        /// <summary>
        /// Distance from prev node
        /// </summary>
        public double ConeLength
        {
            get
            {
                return mConeLength;
            }

            set
            {
                mConeLength = value;
            }
        }

        /// <summary>
        /// Shift from parent node
        /// </summary>
        public double ConeShift
        {
            get
            {
                return mConeShift;
            }

            set
            {
                mConeShift = value;
            }
        }

        /// <summary>
        /// Angle from parent node
        /// </summary>
        public double ConeAngle
        {
            get
            {
                return mConeAngle;
            }

            set
            {
                mConeAngle = value;
            }
        }

        /// <summary>
        /// If edges going out of this node
        /// are hidden
        /// </summary>
        public bool EdgesCollapsed
        {
            get
            {
                return edgesCollapsed;
            }

            set
            {
                edgesCollapsed = value;
            }
        }

        /// <summary>
        /// If this node is hiddent
        /// </summary>
        public bool NodeCollapsed
        {
            get
            {
                return nodeCollapsed;
            }

            set
            {
                nodeCollapsed = value;
            }
        }

        /// <summary>
        /// Direction in which this node is to be rendered
        /// </summary>
        public Vector3D Dir
        {
            get
            {
                return mDir;
            }

            set
            {
                mDir = value;
            }
        }

        /// <summary>
        /// Perpedicular to direction
        /// </summary>
        public Vector3D Cross
        {
            get
            {
                return mCross;
            }

            set
            {
                mCross = value;
            }
        }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">Nodes id</param>
        /// <param name="name">Name to show when highlighted</param>
        /// <param name="infoLabel">Label, where nodes info is showed</param>
        public Node(string id, string name, Label infoLabel)
        {
            mId = id;
            mGeometry = new CubeBase(this, name, infoLabel);
            mR = mGeometry.A;
        }

        /// <summary>
        /// Add new node as this nodes child
        /// </summary>
        public void AddOutEdges(Node node)
        {
            mOutEdges.Add(node);
        }

        /// <summary>
        /// Set nodes parent
        /// </summary>
        /// <param name="parent"></param>
        public void setParent(Node parent)
        {
            mInEdge = parent;
        }

        /// <summary>
        /// Add nodes parent
        /// </summary>
        public void AddInEdges(Node node)
        {
            mInEdges.Add(node);
        }

        /// <summary>
        /// Add edges geometry(for nodes collapsing)
        /// </summary>
        public void AddOutEdgeGeometry(GeometryModel3D edge)
        {
            outEdgesGeometry.Children.Add(edge);
        }

        /// <summary>
        /// Add edges geometry(for nodes collapsing)
        /// </summary>
        public void AddAdditionalEdgeGeometry(string id, GeometryModel3D edge)
        {
            additionalEdgesGeometry[id] = edge;
        }

        /// <summary>
        /// Add additional edges
        /// </summary>
        /// <param name="node"></param>
        public void AddAdditionalInEdges(Node node)
        {
            mAdditionalInEdges.Add(node);
        }

        /// <summary>
        /// Add additional out edges
        /// </summary>
        /// <param name="node"></param>
        public void AddAdditionalOutEdges(Node node)
        {
            mAdditionalOutEdges.Add(node);
        }

        /// <summary>
        /// Remove edge owned by this node
        /// </summary>
        public void RemoveOutEdges(List <Node> nodes)
        {
            mOutEdges.RemoveAll(n => nodes.Contains(n));
        }

        /// <summary>
        /// Remove edge not owned by this node
        /// </summary>
        public void RemoveInEdge(Node node)
        {
            mInEdges.Remove(node);
        }

        /// <summary>
        /// Create nodes 3D model
        /// </summary>
        public void CreateModel()
        {
            TranslateTransform3D translate = new TranslateTransform3D(mPosition.X, mPosition.Y, mPosition.Z);

            mGeometry.CreateGeometry(translate);
        }

        /// <summary>
        /// Is node a leaf(has no children)
        /// </summary>
        public bool isLeaf()
        {
            return mChildren == 0;
        }

        /// <summary>
        /// Set new nodes position
        /// </summary>
        public void UpdateTransformation()
        {
            TranslateTransform3D translate = new TranslateTransform3D(mPosition.X, mPosition.Y, mPosition.Z);
            mGeometry.Geometry.Transform = translate;
        }

        /// <summary>
        /// Node clicked for manipulation
        /// </summary>
        public void MouseRightClick(object sender, MouseButtonEventArgs e)
        {
            Camera.Instance().Freeze();
            NodeManipulator.Instance().SetNode(this);
            mGeometry.Focus();
        }

        /// <summary>
        /// User stoped manipulation
        /// </summary>
        public void MouseRightUnclick()
        {
            Camera.Instance().Unfreeze();
            mGeometry.Unfocus();
        }

    }
}
