﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Threading;

namespace Walrus
{
    class Graph
    {
        private Node root;
        private double mR = 0;
        private int mFilter = 50;

        ContainerUIElement3D mContainer = new ContainerUIElement3D();
        Model3DGroup edgesContainer = new Model3DGroup();

        public int EdgesNr { get; private set; } = 0;
        public int NodesNr { get; private set; } = 0;

        public Graph()
        {
            NodeCollapser.Instance().setData(mContainer, edgesContainer);
            NodeManipulator.Instance().setEdgesContainer(edgesContainer);
        }

        /// <summary>
        /// Graphs radius
        /// </summary>
        public double R
        {
            get
            {
                return (mR + 0.5);
            }
        }

        /// <summary>
        /// Filter for collapsing nodes
        /// </summary>
        public int Filter
        {
            get
            {
                return mFilter;
            }

            set
            {
                mFilter = value;
            }
        }

        /// <summary>
        /// Load graph from graphml file
        /// </summary>
        public void LoadGraph(string file, Label infoLabel)
        {
            GraphParser parser = new GraphParser();
            root = parser.LoadGraph(file, infoLabel);

            EdgesNr = parser.EdgesNr;
            NodesNr = parser.NodesNr;
        }

        /// <summary>
        /// Find spanning tree of graph
        /// </summary>
        public void FindSpanningTree()
        {
            if (EdgesNr + 1 != NodesNr)
            {
                //not a tree graph
                SpanningTreeSeeker sts = new SpanningTreeSeeker(root);
                sts.findSpanningTree(root);

                //Tree graphs have parents set when loading from file
            }
        }

        /// <summary>
        /// calculates position of the nodes
        /// </summary>
        public void Render()
        {
            List<Node> levelOwners;
            
            LevelOwnersFinder levelsFinder = new LevelOwnersFinder();
            levelOwners = levelsFinder.findLevelOwners(root);

            DiameterCounter dCounter = new DiameterCounter();
            dCounter.CountDameter(root);

            GraphRenderer renderer = new GraphRenderer();
            renderer.renderGraph(root);

            mR = renderer.MaxDistance;
            Camera.Instance().maxR = mR + 8.0;
        }

        /// <summary>
        /// return nodes geometry as container
        /// </summary>
        public ContainerUIElement3D getGeometry()
        {
            if(root != null)
                geometryGetterDSF(root);
            return mContainer;
        }

        /// <summary>
        /// return edges geometry as container
        /// </summary>
        public Model3DGroup getEdgesGeometry()
        {
            edgesGeometryGetterDSF(root);
            return edgesContainer;
        }

        /// <summary>
        /// Collapse nodes with given nr of child
        /// </summary>
        public void applyFilter()
        {
            NodeCollapser.Instance().setFilter(mFilter);
            NodeCollapser.Instance().Collapse(root);
        }
        
        /// <summary>
        /// Create nodes model and add it to 
        /// geometry container
        /// </summary>
        private void geometryGetterDSF(Node n)
        {
            n.CreateModel();
            mContainer.Children.Add(n.Geometry.Model);
            foreach(Node child in n.outEdges)
            {
                geometryGetterDSF(child);
            }
        }

        /// <summary>
        /// Create edge model and add it to
        /// edges geometry containter
        /// </summary>
        private void edgesGeometryGetterDSF(Node n)
        {
            foreach (Node additional in n.additionalOutEdges)
            {
                GeometryModel3D addEdge = GeometryBuilder.BuildLine(
                   n.Position, additional.Position,
                   new DiffuseMaterial(new SolidColorBrush(Colors.Purple)));

                edgesContainer.Children.Add(addEdge);
                n.AddAdditionalEdgeGeometry(additional.Id, addEdge);
            }

            foreach (Node child in n.outEdges)
            {
            
                GeometryModel3D edge = GeometryBuilder.BuildLine(
                        n.Position, child.Position,
                        new DiffuseMaterial(new SolidColorBrush(Colors.Purple)));

                edgesContainer.Children.Add(edge);
                n.AddOutEdgeGeometry(edge);
     
                edgesGeometryGetterDSF(child);
            }


        }

    }
}