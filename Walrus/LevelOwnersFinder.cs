﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Walrus
{
    class LevelOwnersFinder
    {
        private List<Node> mLevelOwners = new List<Node>();

        /// <summary>
        /// Counts each node numer of leaf, total number of children
        /// and return list of levels owners
        /// </summary>
        public List<Node> findLevelOwners(Node root)
        {
            List<Thread> threads = new List<Thread>();
            List<Finder> finders = new List<Finder>();

            //start new thread for every roots child
            foreach (var child in root.outEdges)
            {
                Finder f = new Finder(child);
                threads.Add(new Thread(new ThreadStart(f.childCountingThread)));
                threads.Last().Start();
                finders.Add(f);

            }
            //wait for threads to finish
            foreach(var countingThread in threads)
            {
                countingThread.Join();
            }

            //collect found levels
            foreach(var f in finders)
            {
                foreach(var level in f.mLevelOwner)
                {
                    mLevelOwners.Add(level);
                }
            }

            //count roots chidlren
            foreach (Node child in root.outEdges)
            {
                root.Children += child.Children;

                if (child.Children == 0)
                {
                    root.Leafs += 1;
                }
            }
          
            Trace.WriteLine("Total nodes nr: " + root.Children.ToString());

            return mLevelOwners;
        }

        /// <summary>
        /// Class handling searching thread
        /// </summary>
        private class Finder
        {
            /// <summary>
            /// Found levels owners
            /// </summary>
            public List<Node> mLevelOwner { get; set; } = new List<Node>();

            /// <summary>
            /// Initial node
            /// </summary>
            public Node node { get; set; }

            public Finder(Node startNode)
            {
                node = startNode;
            }

            /// <summary>
            /// Thread start function
            /// </summary>
            public void childCountingThread()
            {
                childCounterDSF(node);
            }

            /// <summary>
            /// Recursively count nodes children
            /// </summary>
            private int childCounterDSF(Node node)
            {
                foreach (Node child in node.outEdges)
                {
                    int nodeChildren = childCounterDSF(child);
                    node.Children += nodeChildren;

                    if (nodeChildren == 1)
                    {
                        node.Leafs += 1;
                    }
                }
                if (node.Leafs == node.Children && node.Leafs != 0)
                {
                    node.LevelOwner = true;
                    mLevelOwner.Add(node);
                    countR(node);
                }

                return node.Children + 1;
            }

            /// <summary>
            /// Count radius of level sphere
            /// </summary>
            private void countR(Node n)
            {
                double r = 0;
                double a = n.Geometry.A;

                int childNr = n.Leafs;

                //estimated maximum area occupied by
                //leafs faces facing level owner
                double maxAreax = 4.0 * (a * a) * childNr;

                //count r of half sphere able to 
                //contain max area:
                /*
                    P = 2 * PI * R^2
                    R = sqrt(P/(2*PI))
                */
                r = Math.Sqrt(maxAreax / (2.0 * Math.PI));
                //smaller r doesnt work
                if (r < 0.0797884560802865)
                {
                    r = 0.0797884560802865;
                }
                if (childNr == 1)
                {
                    r = 2 * n.Geometry.A;
                }
                n.R = r;
            }
        }

      
    }
}
