﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Walrus
{
    class DiameterCounter
    {

        /// <summary>
        /// count position of every node in regards to its parent
        /// </summary>
        public void CountDameter(Node root)
        {
            //create and start thread for every branch
            List<Thread> threads = new List<Thread>();
            foreach(var child in root.outEdges)
            {
                if (child.isLeaf())
                    continue;

                DiameterThread dt = new DiameterThread(child);
                threads.Add(new Thread(new ThreadStart(dt.countDiameter)));
                threads.Last().Start();
            }
            //wait for threads to finnish
            foreach(var thread in threads)
            {
                thread.Join();
            }
           
            foreach(Node c in root.outEdges)
            {
                c.ConeShift = 0.0001;
                c.ConeAngle = 0.0;

                c.ConeLength *= 3;
            }
        }

        private class DiameterThread
        {
            #region constants
            const double maxDeltaR = 0.015;
            const double lenghtPerChild = 0.02;
            const double minLength = 0.04;
            const double maxLenth = 0.2;
            #endregion

            private Node startNode;
            public DiameterThread(Node start)
            {
                startNode = start;
            }

            public void countDiameter()
            {
                setDSF(startNode);
            }

            /// <summary>
            /// count nodes distance from parent
            /// </summary>
            private static void setLength(Node n)
            {
                n.ConeLength = (n.Children - n.Leafs) * lenghtPerChild;
                if (n.ConeLength < minLength)
                    n.ConeLength = minLength;
                if (n.ConeLength > maxLenth)
                    n.ConeLength = maxLenth;
            }

            /// <summary>
            /// use Depth Search First algorithm to 
            /// count position of every node in regards to its parent
            /// </summary>
            private void setDSF(Node n)
            {
                //leaf of sub-level
                if (n.Children == 0)
                {
                    n.R = n.Geometry.A;
                    return;
                }


                //  level owner
                if (n.LevelOwner)
                {
                    n.ConeLength = (n.Children) * lenghtPerChild;
                    if (n.ConeLength < minLength)
                        n.ConeLength = minLength;
                    if (n.ConeLength > maxLenth)
                        n.ConeLength = maxLenth;
                    return;
                }

                foreach (Node c in n.outEdges)
                {
                    setDSF(c);
                }

                //  1 child
                if ((n.outEdges.Count - n.Leafs) == 1)
                {
                    //make it possible for user to manipulate only child
                    //render very close to center
                    n.outEdges[0].ConeAngle = 0;
                    n.outEdges[0].ConeShift = 0.0001;

                    n.R = n.outEdges[0].R;
                    setLength(n);

                    return;
                }


                //many different children
                n.outEdges.Sort((x, y) => -1 * x.R.CompareTo(y.R));
                double maxR = n.outEdges[0].R;
                {
                    /*
                        Wielokąt foremny -  regular polygon
                        okrąg opisany - excircle
                        wierzchołek - vertex
                        bok - edge

                        Every child node has the same radius equal to the greatest radius of 
                        all children. We construct regular polygon of edge lenght equal 2*child_radius. 
                        Then we construct excircle on it and get R. Then draw our nodes in vertices of polygon.
                    */

                    //formula from https://pl.wikipedia.org/wiki/Wielokąt_foremny
                    double currentR = (1.5 * maxR) / (2.0 * Math.Sin(Math.PI / (double)n.outEdges.Count - n.Leafs));
                    /*
                        Ob = 2 * PI * R
                        R = Ob/ (2 * PI)
                    */
                    double minR = ((n.outEdges.Count - n.Leafs) * 2.0 * maxR) / (2.0 * Math.PI);

                    double deltaAngle = 2.0 * Math.PI / ((n.outEdges.Count - n.Leafs));
                    double maxConeLength = n.outEdges[0].ConeLength;


                    //find smallest posible radius
                    if ((minR + 0.02) < currentR)
                    {
                        double distance2 = maxConeLength * maxConeLength + currentR * currentR;
                        maxConeLength = Math.Sqrt(distance2 - minR * minR);
                        currentR = minR;
                    }

                    double maxLenth = 0.0;

                    foreach (Node child in n.outEdges)
                    {
                        if (child.outEdges.Count == 0)
                            continue;

                        child.R = maxR;
                        child.ConeLength = maxConeLength;
                        child.ConeShift = currentR;
                        child.ConeAngle = deltaAngle;

                        maxLenth = (child.ConeLength > maxLenth ? child.ConeLength : maxLenth);
                    }

                    setLength(n);
                    n.ConeShift = 0;

                    currentR += maxR;
                    n.R = (currentR * n.ConeLength) / (double)(n.ConeLength + maxLenth);
                }
            }

        }


    }
}
