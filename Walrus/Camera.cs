﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace Walrus
{
    class Camera
    {
        private const double DEFAULT_R = 7.5;
        private const double DEFAULT_LATITUDE = System.Math.PI / 2 - System.Math.PI / 16;
        private const double DEFAULT_LONGITUDE = -System.Math.PI / 6;

        private double mCameraR;
        private double mLatitude;
        private double mLongitude;

        private Point3D mCenter = new Point3D(0, 0, 0);
        private PerspectiveCamera mCamera;

        private bool mFreezed = false;

        public double maxR { get; set; } = 8;

        #region singleton

        private static Camera mInstance = null;

        /// <summary>
        /// Camera is singleton
        /// </summary>
        public static Camera Instance()
        {
            if (mInstance == null)
            {
                mInstance = new Camera();
            }

            return mInstance;
        }

        /// <summary>
        /// Private constructor
        /// </summary>
        private Camera()
        {

        }

        #endregion

        /// <summary>
        /// Set wpf3d perspective camera
        /// </summary>
        public void setCamera(PerspectiveCamera camera)
        {
            mCamera = camera;
            Reset();
        }

        /// <summary>
        /// Update camera position
        /// </summary>
        private void setCamera()
        {
            mCamera.Position = new Point3D(
                mCameraR * System.Math.Cos(mLongitude) * System.Math.Sin(mLatitude) - mCenter.X,
                mCameraR * System.Math.Cos(mLatitude) - mCenter.Y,
                mCameraR * System.Math.Sin(mLongitude) * System.Math.Sin(mLatitude) - mCenter.Z);

            mCamera.LookDirection = new Vector3D(
                (mCameraR - 10) * System.Math.Cos(mLongitude) * System.Math.Sin(mLatitude) - mCenter.X,
                (mCameraR - 10) * System.Math.Cos(mLatitude) - mCenter.Y,
                (mCameraR - 10) * System.Math.Sin(mLongitude) * System.Math.Sin(mLatitude) - mCenter.Z);
        }

        /// <summary>
        /// Reset camera position
        /// </summary>
        public void Reset()
        {
            mCameraR = DEFAULT_R;
            mLatitude = DEFAULT_LATITUDE;
            mLongitude = DEFAULT_LONGITUDE;

            mCenter = new Point3D(0, 0, 0);

            setCamera();
        }

        /// <summary>
        /// Rotate camera around center point
        /// </summary>
        public void Rotate(double dx, double dy)
        {
            if (mFreezed)
                return;

            mLongitude += dx;

            mLatitude += dy;
            if (mLatitude < 0.00333333333333333)
                mLatitude = 0.00333333333333333;
            if (mLatitude > Math.PI)
                mLatitude = Math.PI;

            setCamera();
        }

        /// <summary>
        /// Change camera radius - results in camera forwar/backward movement
        /// </summary>
        public void changeR(double delta)
        {
            if (mFreezed)
                return;

            mCameraR -= delta / 1000D;
            if (mCameraR > maxR)
                mCameraR = maxR;

            setCamera();
        }

        /// <summary>
        /// Change camera center point
        /// </summary>
        public void Move(double dx, double dy)
        {
            if (mFreezed)
                return;

            dx /= 3000 / (mCameraR > 1 ? mCameraR : 1.0 );
            dy /= 3000 / (mCameraR > 1 ? mCameraR : 1.0);

            mCenter.X += Math.Sin(mLongitude) *dx;
            mCenter.Z -= Math.Cos(mLongitude) * dx;
            mCenter.Y += dy;

            setCamera();
        }

        /// <summary>
        /// Prevent any camera movement
        /// </summary>
        public void Freeze()
        {
            mFreezed = true;
        }

        /// <summary>
        /// allow camera to move
        /// </summary>
        public void Unfreeze()
        {
            mFreezed = false;
        }
    }
}
