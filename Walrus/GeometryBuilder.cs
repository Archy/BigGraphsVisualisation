﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace Walrus
{
    /// <summary>
    ///  This class creates specified 3d models 
    /// </summary>
    static class GeometryBuilder
    {
        static GeometryBuilder()
        {
            //create line mesh
            double a = 0.005;

            // Define 3D mesh object
            sLineMesh = new MeshGeometry3D();

            double h = Math.Sqrt(3) * (a / 2.0) / 2.0;

            // Front face
            sLineMesh.Positions.Add(new Point3D(0, -h, (a / 2)));
            sLineMesh.Positions.Add(new Point3D(0, h, 0));
            sLineMesh.Positions.Add(new Point3D(0, -h, -(a / 2)));

            // Back face
            sLineMesh.Positions.Add(new Point3D((sLineLenght), -h, (a / 2)));
            sLineMesh.Positions.Add(new Point3D((sLineLenght), h, 0));
            sLineMesh.Positions.Add(new Point3D((sLineLenght), -h, -(a / 2)));

            // Front face
            sLineMesh.TriangleIndices.Add(0);
            sLineMesh.TriangleIndices.Add(3);
            sLineMesh.TriangleIndices.Add(4);
            sLineMesh.TriangleIndices.Add(0);
            sLineMesh.TriangleIndices.Add(4);
            sLineMesh.TriangleIndices.Add(1);
            // Back face
            sLineMesh.TriangleIndices.Add(0);
            sLineMesh.TriangleIndices.Add(2);
            sLineMesh.TriangleIndices.Add(5);
            sLineMesh.TriangleIndices.Add(0);
            sLineMesh.TriangleIndices.Add(5);
            sLineMesh.TriangleIndices.Add(3);
            // Bottom face
            sLineMesh.TriangleIndices.Add(5);
            sLineMesh.TriangleIndices.Add(2);
            sLineMesh.TriangleIndices.Add(1);
            sLineMesh.TriangleIndices.Add(5);
            sLineMesh.TriangleIndices.Add(1);
            sLineMesh.TriangleIndices.Add(4);
        }
        private static MeshGeometry3D sLineMesh;
        private static double sLineLenght = 0.1;

        private static MeshGeometry3D sCubeMesh = new MeshGeometry3D();
        private static double sPrevA = 0.0;
        private static void CreateCubeMesh(double a)
        {
            // Define 3D mesh object
            MeshGeometry3D mesh = sCubeMesh;
          
            // Front face
            mesh.Positions.Add(new Point3D(-a, -a, a));
            mesh.Positions.Add(new Point3D(a, -a, a));
            mesh.Positions.Add(new Point3D(a, a, a));
            mesh.Positions.Add(new Point3D(-a, a, a));
            // Back face
            mesh.Positions.Add(new Point3D(-a, -a, -a));
            mesh.Positions.Add(new Point3D(a, -a, -a));
            mesh.Positions.Add(new Point3D(a, a, -a));
            mesh.Positions.Add(new Point3D(-a, a, -a));

            // Front face
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(3);
            mesh.TriangleIndices.Add(0);
            // Back face
            mesh.TriangleIndices.Add(6);
            mesh.TriangleIndices.Add(5);
            mesh.TriangleIndices.Add(4);
            mesh.TriangleIndices.Add(4);
            mesh.TriangleIndices.Add(7);
            mesh.TriangleIndices.Add(6);
            // Right face
            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(5);
            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(5);
            mesh.TriangleIndices.Add(6);
            mesh.TriangleIndices.Add(2);
            // Top face
            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(6);
            mesh.TriangleIndices.Add(3);
            mesh.TriangleIndices.Add(3);
            mesh.TriangleIndices.Add(6);
            mesh.TriangleIndices.Add(7);
            // Bottom face
            mesh.TriangleIndices.Add(5);
            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(4);
            mesh.TriangleIndices.Add(5);
            // Right face
            mesh.TriangleIndices.Add(4);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(3);
            mesh.TriangleIndices.Add(3);
            mesh.TriangleIndices.Add(7);
            mesh.TriangleIndices.Add(4);
        }

        private static MeshGeometry3D sTetraedrMesh = new MeshGeometry3D();
        private static double sPrevTetrA = 0.0;
        private static void CreateTetraedr(double a)
        {
            double h = a * Math.Sqrt(6) / 3.0;
            double tBase = a * Math.Sqrt(3) / 2.0;


            // Define 3D mesh object
            sTetraedrMesh.Positions.Add(new Point3D(-(a/2), -(h/3), (tBase / 3)));
            sTetraedrMesh.Positions.Add(new Point3D((a / 2), -(h / 3), (tBase / 3)));
            sTetraedrMesh.Positions.Add(new Point3D(0, 2*(h / 3), 0));
            sTetraedrMesh.Positions.Add(new Point3D(0, -(h / 3), -2*(tBase / 3)));

            // Front face
            sTetraedrMesh.TriangleIndices.Add(0);
            sTetraedrMesh.TriangleIndices.Add(1);
            sTetraedrMesh.TriangleIndices.Add(2);
            // Left face
            sTetraedrMesh.TriangleIndices.Add(3);
            sTetraedrMesh.TriangleIndices.Add(0);
            sTetraedrMesh.TriangleIndices.Add(2);
            // Right face
            sTetraedrMesh.TriangleIndices.Add(1);
            sTetraedrMesh.TriangleIndices.Add(3);
            sTetraedrMesh.TriangleIndices.Add(2);
            // Bottom face
            sTetraedrMesh.TriangleIndices.Add(3);
            sTetraedrMesh.TriangleIndices.Add(1);
            sTetraedrMesh.TriangleIndices.Add(0);
        }
        
        /// <summary>
        ///  Build cube of given edge width
        /// </summary>
        public static GeometryModel3D BuildCube(double a, MaterialGroup material, TranslateTransform3D tranform = null)
        {
            //dont create the same model 2 times
            if(a != sPrevA)
            {
                CreateCubeMesh(a);
                sPrevA = a;
            }

            // Geometry creation
            sCubeMesh.Freeze();
            GeometryModel3D geometry = new GeometryModel3D(sCubeMesh, material);
            if (tranform != null)
            {
                geometry.Transform = tranform;
            }
            return geometry;
        }
        
        /// <summary>
        ///  Build tetraedr of given edge width
        /// </summary>
        public static GeometryModel3D BuildTetraedr(double a, MaterialGroup material, TranslateTransform3D tranform = null)
        {
            //dont create the same model 2 times
            if (a != sPrevTetrA)
            {
                CreateTetraedr(sPrevTetrA);
                sPrevTetrA = a;
            }

            // Geometry creation
            sTetraedrMesh.Freeze();
            GeometryModel3D geometry = new GeometryModel3D(sTetraedrMesh, material);
            if (tranform != null)
            {
                geometry.Transform = tranform;
            }
            return geometry;
        }

        /// <summary>
        ///  Build ring of given radius and with around x axis
        /// </summary>
        public static GeometryModel3D BuildRingX(double R, double WIDTH, int N, DiffuseMaterial material)
        {
            MeshGeometry3D mesh = new MeshGeometry3D();
            
            double DA = (2 * System.Math.PI) / N;
            const int POINTS_PER_ITERATION = 8;

            double angle = 0;
            int i;

            for (i = 0; i < N; ++i, angle += DA)
            {
                mesh.Positions.Add(new Point3D(R * System.Math.Cos(angle), 0, R * System.Math.Sin(angle)));
                mesh.Positions.Add(new Point3D(R * System.Math.Cos(angle + DA), 0, R * System.Math.Sin(angle + DA)));
                mesh.Positions.Add(new Point3D((R + WIDTH) * System.Math.Cos(angle), 0, (R + WIDTH) * System.Math.Sin(angle)));
                mesh.Positions.Add(new Point3D((R + WIDTH) * System.Math.Cos(angle + DA), 0, (R + WIDTH) * System.Math.Sin(angle + DA)));

                //perpendicular:
                mesh.Positions.Add(new Point3D((R + WIDTH / 2) * System.Math.Cos(angle), WIDTH / 2, (R + WIDTH / 2) * System.Math.Sin(angle)));
                mesh.Positions.Add(new Point3D((R + WIDTH / 2) * System.Math.Cos(angle), -WIDTH / 2, (R + WIDTH / 2) * System.Math.Sin(angle)));
                mesh.Positions.Add(new Point3D((R + WIDTH / 2) * System.Math.Cos(angle + DA), WIDTH / 2, (R + WIDTH / 2) * System.Math.Sin(angle + DA)));
                mesh.Positions.Add(new Point3D((R + WIDTH / 2) * System.Math.Cos(angle + DA), -WIDTH / 2, (R + WIDTH / 2) * System.Math.Sin(angle + DA)));

                //######################################################
                //INDICIES:

                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 0);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 1);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 3);

                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 3);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 2);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 0);

                //reversed:
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 3);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 1);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 0);

                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 0);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 2);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 3);

                //perpendicular:
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 4);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 5);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 7);

                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 7);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 6);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 4);

                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 7);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 5);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 4);

                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 4);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 6);
                mesh.TriangleIndices.Add(i * POINTS_PER_ITERATION + 7);
      
            }

            // Geometry creation
            return new GeometryModel3D(mesh, material);
        }

        /// <summary>
        ///  Build ring of given radius and with around y axis
        /// </summary>
        public static GeometryModel3D BuildRingY(double R, double WIDTH, int N, DiffuseMaterial material)
        {
            MeshGeometry3D mesh2 = new MeshGeometry3D();

            double DA = (2 * System.Math.PI) / N;
            const int POINTS_PER_ITERATION = 8;

            double angle = 0;
            int i;

            for (i = 0; i < N; ++i, angle += DA)
            {
                mesh2.Positions.Add(new Point3D(R * System.Math.Cos(angle), R * System.Math.Sin(angle), 0));
                mesh2.Positions.Add(new Point3D(R * System.Math.Cos(angle + DA), R * System.Math.Sin(angle + DA), 0));
                mesh2.Positions.Add(new Point3D((R + WIDTH) * System.Math.Cos(angle), (R + WIDTH) * System.Math.Sin(angle), 0));
                mesh2.Positions.Add(new Point3D((R + WIDTH) * System.Math.Cos(angle + DA), (R + WIDTH) * System.Math.Sin(angle + DA), 0));

                //perpendicular:
                mesh2.Positions.Add(new Point3D((R + WIDTH / 2) * System.Math.Cos(angle), (R + WIDTH / 2) * System.Math.Sin(angle), WIDTH / 2));
                mesh2.Positions.Add(new Point3D((R + WIDTH / 2) * System.Math.Cos(angle), (R + WIDTH / 2) * System.Math.Sin(angle), -WIDTH / 2));
                mesh2.Positions.Add(new Point3D((R + WIDTH / 2) * System.Math.Cos(angle + DA), (R + WIDTH / 2) * System.Math.Sin(angle + DA), WIDTH / 2));
                mesh2.Positions.Add(new Point3D((R + WIDTH / 2) * System.Math.Cos(angle + DA), (R + WIDTH / 2) * System.Math.Sin(angle + DA), -WIDTH / 2));

                //######################################################
                //INDICIES:

                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 0);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 1);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 3);

                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 3);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 2);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 0);

                //reversed:
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 3);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 1);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 0);

                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 0);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 2);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 3);

                //perpendicular:
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 4);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 5);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 7);

                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 7);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 6);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 4);

                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 7);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 5);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 4);

                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 4);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 6);
                mesh2.TriangleIndices.Add(i * POINTS_PER_ITERATION + 7);
            }

            return new GeometryModel3D(mesh2, material);
        }

        /// <summary>
        ///  Build ring of given radius and with around z axis
        /// </summary>
        public static GeometryModel3D BuildRingZ(double R, double WIDTH, int N, DiffuseMaterial material)
        {
            MeshGeometry3D mesh3 = new MeshGeometry3D();

            double DA = (2 * System.Math.PI) / N;
            const int POINTS_PER_ITERATION = 8;

            double angle = 0;
            int i;

            for (i = 0; i < N; ++i, angle += DA)
            {
                mesh3.Positions.Add(new Point3D(0, R * System.Math.Cos(angle), R * System.Math.Sin(angle)));
                mesh3.Positions.Add(new Point3D(0, R * System.Math.Cos(angle + DA), R * System.Math.Sin(angle + DA)));
                mesh3.Positions.Add(new Point3D(0, (R + WIDTH) * System.Math.Cos(angle), (R + WIDTH) * System.Math.Sin(angle)));
                mesh3.Positions.Add(new Point3D(0, (R + WIDTH) * System.Math.Cos(angle + DA), (R + WIDTH) * System.Math.Sin(angle + DA)));

                //perpendicular:
                mesh3.Positions.Add(new Point3D(WIDTH / 2, (R + WIDTH / 2) * System.Math.Cos(angle), (R + WIDTH / 2) * System.Math.Sin(angle)));
                mesh3.Positions.Add(new Point3D(-WIDTH / 2, (R + WIDTH / 2) * System.Math.Cos(angle), (R + WIDTH / 2) * System.Math.Sin(angle)));
                mesh3.Positions.Add(new Point3D(WIDTH / 2, (R + WIDTH / 2) * System.Math.Cos(angle + DA), (R + WIDTH / 2) * System.Math.Sin(angle + DA)));
                mesh3.Positions.Add(new Point3D(-WIDTH / 2, (R + WIDTH / 2) * System.Math.Cos(angle + DA), (R + WIDTH / 2) * System.Math.Sin(angle + DA)));

                //######################################################
                //INDICIES:

                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 0);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 1);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 3);

                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 3);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 2);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 0);

                //reversed:
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 3);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 1);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 0);

                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 0);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 2);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 3);

                //perpendicular:
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 4);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 5);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 7);

                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 7);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 6);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 4);

                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 7);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 5);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 4);

                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 4);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 6);
                mesh3.TriangleIndices.Add(i * POINTS_PER_ITERATION + 7);
            }

            return new GeometryModel3D(mesh3, material);
        }

        /// <summary>
        ///  Build line from point to point
        /// </summary>
        public static GeometryModel3D BuildLine(Point3D a, Point3D b, DiffuseMaterial material)
        {
            //magic
            Vector3D l = new Vector3D(sLineLenght, 0, 0);
            Vector3D v = new Vector3D(b.X-a.X, b.Y - a.Y, b.Z - a.Z);
            Transform3DGroup transformGroup = new Transform3DGroup();

            ScaleTransform3D scale = new ScaleTransform3D(v.Length/sLineLenght,1,1);
            transformGroup.Children.Add(scale);

            TranslateTransform3D tranform = new TranslateTransform3D(a.X, a.Y, a.Z);
            transformGroup.Children.Add(tranform);

            Vector3D rotAxis = Vector3D.CrossProduct(l, v);
            double cosAngle = Vector3D.DotProduct(l, v) / (v.Length * l.Length);
            double angle = Math.Acos(cosAngle) * (180.0 / Math.PI);
            Rotation3D rotationXZ = new AxisAngleRotation3D(rotAxis, angle);
            RotateTransform3D rotateTranformXZ = new RotateTransform3D(rotationXZ, a);
            transformGroup.Children.Add(rotateTranformXZ);

            sLineMesh.Freeze();
            GeometryModel3D geometry = new GeometryModel3D(sLineMesh, material);
            geometry.Transform = transformGroup;
            geometry.Freeze();
            return geometry;
        }
    }
}
