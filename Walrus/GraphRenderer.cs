﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Media3D;

namespace Walrus
{
    class GraphRenderer
    {
        /// <summary>
        /// Nodes maximal distance from point(0,0,0)
        /// </summary>
        public double MaxDistance { get; private set; } = 0;
       
        /// <summary>
        /// Set exact postion of every noode. Start with root.
        /// </summary>
        public void renderGraph(Node root)
        {
            //threads for rendering graph branches simultaneusly
            List<Thread> threads = new List<Thread>();
            List<RenderThread> renderers = new List<RenderThread>();

            if (root.Children == 0)
                return;

            int childNr = root.Children;
            List<Point> angles = SphereDistributor.getDistribution(root.outEdges.Count - root.Leafs);
            
            const double R = 1;
            int n = 0;
            //render children in descending order comparing their radiuses

            root.outEdges.Sort((x,y) => -1*x.Children.CompareTo(y.Children));
            foreach (Node c in root.outEdges)
            {
                if (c.outEdges.Count == 0)
                    continue;

                double currAngleXZ = angles[n].Y;
                double currAngleY = angles[n].X;
                
                double x = R * Math.Cos(currAngleXZ) * Math.Sin(currAngleY);
                double z = R * Math.Sin(currAngleXZ) * Math.Sin(currAngleY);
                double y = R * Math.Cos(currAngleY);

                RenderThread r = new RenderThread(c, new Vector3D(x, y, z), new Vector3D(1, 0, 1));
                renderers.Add(r);
                threads.Add(new Thread(new ThreadStart(r.render)));
                threads.Last().Start();

                ++n;
            }

            if(root.Leafs > 0)
            {
                RenderThread r = new RenderThread();
                r.renderSubnodeLeafs(root, new Vector3D(0,1,0), 0.1);
            }

            foreach(var thread in threads)
            {
                thread.Join();
            }
            foreach(var r in renderers)
            {
                if(MaxDistance < r.mLevelRender.MaxDistance)
                {
                    MaxDistance = r.mLevelRender.MaxDistance;
                }
            }
        }

        /// <summary>
        /// Re-render node and its children
        /// </summary>
        public void renderNode(Node n, Vector3D dir, Vector3D vertShift)
        {
            //set initial node new position
            RenderThread renderer = new RenderThread();
            renderer.setNodesPosition(n, dir, vertShift);

            //count new direction for children and perpendicular to it
            Vector3D newDir = new Vector3D(n.Position.X - n.Parent.Position.X, 
                n.Position.Y - n.Parent.Position.Y, n.Position.Z - n.Parent.Position.Z);
            newDir.Normalize();
            Vector3D newCross = Vector3D.CrossProduct(
                newDir, new Vector3D(1, (newDir.Y == 1 ? -1 : 1), 1));
            newCross.Normalize();

            //render level 
            if (n.LevelOwner)
            {
                LevelRenderer lr = new LevelRenderer();
                lr.renderLevel(n, newDir);
            }
            else
            {
                //render children
                List<Thread> threads = new List<Thread>();
                foreach (var child in n.outEdges)
                {
                    RenderThread r = new RenderThread(child, newDir, newCross);
                    threads.Add(new Thread(new ThreadStart(r.render)));
                    threads.Last().Start();

                    Rotation3D rotation = new AxisAngleRotation3D(newDir, child.ConeAngle * (180 / Math.PI));
                    RotateTransform3D rotateTranform = new RotateTransform3D(rotation, n.Position);
                    newCross = rotateTranform.Transform(newCross);
                }

                foreach (var thread in threads)
                {
                    thread.Join();
                }

                //render leafs
                if(n.Leafs > 0)
                {
                    renderer.renderSubnodeLeafs(n, newDir, n.R);
                }
            }

        }

        /// <summary>
        /// Class for hadnling rendering thread.
        /// Unique for every branch comming out of inital node(root)
        /// </summary>
        private class RenderThread
        {
            public LevelRenderer mLevelRender { get; } = new LevelRenderer();

            private Node mStart;
            private Vector3D mStartDir;
            private Vector3D mStartShift;

            /// <summary>
            /// Inital data for rendering
            /// </summary>
            public RenderThread(Node start, Vector3D startDir, Vector3D vertShift)
            {
                mStart = start;
                mStartDir = startDir;
                mStartShift = vertShift;
            }
            public RenderThread()
            {

            }

            /// <summary>
            /// Thread start function
            /// </summary>
            public void render()
            {
                renderNode(mStart, mStartDir, mStartShift);
            }
            
            /// <summary>
            /// Set node new position
            /// </summary>
            public void setNodesPosition(Node n, Vector3D dir, Vector3D vertShift)
            {
                //count position of actual node "n" in regard to its parent
                dir.Normalize();
                Vector3D shift = Vector3D.Multiply(dir, n.ConeLength);
                if (vertShift.Length > 0)
                    vertShift.Normalize();

                vertShift = Vector3D.Multiply(n.ConeShift, vertShift);
                n.Position = new Point3D(n.Parent.Position.X + shift.X + vertShift.X,
                    n.Parent.Position.Y + shift.Y + vertShift.Y,
                    n.Parent.Position.Z + shift.Z + vertShift.Z);

                //remember direction vector and perpendicular to it
                if (vertShift.Length > 0)
                    vertShift.Normalize();
                n.Cross = new Vector3D(vertShift.X, vertShift.Y, vertShift.Z);
                n.Dir = new Vector3D(dir.X, dir.Y, dir.Z);
            }

            /// <summary>
            /// Render subnodes (node with both subnodes and leafs) leafs
            /// around given "dir" vector
            /// </summary>
            public void renderSubnodeLeafs(Node n, Vector3D dir, double R)
            {
                //perpendicular do direction vector
                Vector3D cross = Vector3D.CrossProduct(dir, new Vector3D(0.1, -dir.Y, 0.1));
                cross.Normalize();

                //count how many leaf per 1 cicle, angle between leafs, etc
                int maxLeafsPerCircle = (int)((2.0 * Math.PI * R) / (n.Geometry.A * 1.5));
                int currenCircleNr = 0;
                double deltaAngle = 2.0 * Math.PI / (double)maxLeafsPerCircle;
                double dirShift = 0.0;
                dir.Normalize();

                int remainingLeafs = n.Leafs;

                //render leafs
                foreach (Node leaf in n.outEdges)
                {
                    if (leaf.outEdges.Count > 0)
                        continue;   //not a leaf

                    Rotation3D rotation = new AxisAngleRotation3D(dir, deltaAngle * (180 / Math.PI));
                    RotateTransform3D rotateTranform = new RotateTransform3D(rotation, n.Position);
                    cross = rotateTranform.Transform(cross);
                    cross.Normalize();
                    cross = Vector3D.Multiply(cross, R);

                    Vector3D shift = Vector3D.Multiply(dirShift, dir);

                    leaf.Position = new Point3D(n.Position.X + cross.X + shift.X,
                        n.Position.Y + cross.Y + shift.Y, n.Position.Z + cross.Z + shift.Z);

                    --remainingLeafs;
                    ++currenCircleNr;
                    if (currenCircleNr == maxLeafsPerCircle)
                    {
                        currenCircleNr = 0;
                        dirShift += 2 * n.Geometry.A;

                        //remaining leafs are evenly distributed (count new angle for them)
                        if (remainingLeafs < maxLeafsPerCircle)
                        {
                            deltaAngle = 2.0 * Math.PI / (double)remainingLeafs;
                        }
                    }
                }
            }

            /// <summary>
            /// Render nodes recursively
            /// </summary>
            public void renderNode(Node n, Vector3D dir, Vector3D vertShift)
            {
                setNodesPosition(n, dir, vertShift);

                //render children
                Vector3D newDir = new Vector3D(n.Position.X - n.Parent.Position.X,
                    n.Position.Y - n.Parent.Position.Y, n.Position.Z - n.Parent.Position.Z);
                if (n.LevelOwner)
                {
                    mLevelRender.renderLevel(n, newDir);
                }
                else
                {
                    //perpendicular to new direction
                    Vector3D cross = Vector3D.CrossProduct(newDir, new Vector3D(1, (newDir.Y == 1 ? -1 : 1), 1));
                    foreach (Node c in n.outEdges)
                    {
                        if (c.outEdges.Count == 0)
                            continue;

                        renderNode(c, newDir, cross);
                        Rotation3D rotation = new AxisAngleRotation3D(newDir, c.ConeAngle * (180 / Math.PI));
                        RotateTransform3D rotateTranform = new RotateTransform3D(rotation, n.Position);
                        cross = rotateTranform.Transform(cross);
                    }
                }
                //render leafs if any. (Not a level!)
                if (n.Leafs > 0 && n.LevelOwner == false)
                {
                    renderSubnodeLeafs(n, new Vector3D((n.Position.X - n.Parent.Position.X),
                        (n.Position.Y - n.Parent.Position.Y),
                        (n.Position.Z - n.Parent.Position.Z)), n.R);
                }
            }
        }
    }
}
