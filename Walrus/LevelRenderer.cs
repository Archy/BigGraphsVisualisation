﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace Walrus
{
    class LevelRenderer
    {
        private double r = 0;   //radius of half sphere
        private double a = 0;
        private Node n = null;

        private double maxDistance = 0;

        /// <summary>
        /// Maximum distance of any rendered leaf from center
        /// </summary>
        public double MaxDistance
        {
            get
            {
                return maxDistance;
            }
        }

        /// <summary>
        /// Set exact position of level leafs.
        /// </summary>
        public void renderLevel(Node levelOwner, Vector3D direction)
        {
            a = levelOwner.Geometry.A;
            n = levelOwner;
            r = levelOwner.R;

            int index = 0;
            int sphreLevel = 1;
            double currentY = r - a / 2;

            direction.Normalize();
            Vector3D v = Vector3D.Multiply(direction, r);
            Vector3D normCross = Vector3D.CrossProduct(new Vector3D(1, -direction.Y, 1), direction);
            normCross.Normalize();

            //postion of first top node
            Point3D pos = new Point3D(n.Position.X + v.X, n.Position.Y + v.Y, n.Position.Z + v.Z);
            n.outEdges[index].Position = pos;
            ++index;

            //count distance from center -> some nodes hava only 1 leaf
            //which is the outermost node 
            Vector3D dist = new Vector3D(pos.X, pos.Y, pos.Z);
            if (dist.Length > maxDistance)
                maxDistance = dist.Length;

            while (index < n.Leafs)
            {
                //count r on current sphere level
                
                double ri = Math.Sqrt(r*r - currentY* currentY) + a/10.0;
                //how many cube will fit on current sphere level
                sphreLevel = (int) (Math.Floor((2*Math.PI*ri) / (1.5*a)));
                if (sphreLevel + index >= n.Leafs)
                    sphreLevel = n.Leafs - index ;

                //angle between consecutive cubes in radians
                double angle =  2*Math.PI / sphreLevel;
                double currAngle = 0;

                //how far up from level owner
                v = Vector3D.Multiply(direction, currentY);

                for (int i = 0; i<sphreLevel; ++i)
                {
                    //how far from center of current sphere level 
                    Vector3D rotV = Vector3D.Multiply(normCross, ri); 

                    // rotate every node around center on current level 
                    Rotation3D rotation = new AxisAngleRotation3D(direction, currAngle * (180 / Math.PI));
                    RotateTransform3D rotateTranform = new RotateTransform3D(rotation, n.Position);
                    rotV = rotateTranform.Transform(rotV);

                    n.outEdges[index].Position = new Point3D(n.Position.X + v.X + rotV.X, n.Position.Y + v.Y + rotV.Y, n.Position.Z + v.Z + rotV.Z);

                    //count distance from center
                    dist = new Vector3D(n.Position.X + v.X + rotV.X, n.Position.Y + v.Y + rotV.Y, n.Position.Z + v.Z + rotV.Z);
                    if (dist.Length > maxDistance)
                        maxDistance = dist.Length;

                    currAngle += angle;
                    ++index;
                    if (index == n.Leafs)
                        break;
                }

                currentY -= a;
            }
        }
    }
}
