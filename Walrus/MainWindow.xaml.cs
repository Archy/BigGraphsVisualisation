﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Media3D;

/*
Source:
WPF 3D:
http://www.codeproject.com/Articles/23332/WPF-3D-Primer
http://www.ucancode.net/WPF-3D-Article-Tutorial-with-Chart-Graphics-CSharp-Code.htm
http://www.codeproject.com/Articles/48940/WPF-D-graph
http://www.c-sharpcorner.com/uploadfile/mheydlauf/creating-a-simple-3d-scene-in-wpf/

Algorithm:    
https://github.com/Bplotka/w3-graph
http://graphdrawing.org/literature/gd-constraints.pdf

graph representation in file:
http://graphml.graphdrawing.org
https://cs.brown.edu/~rt/gdhandbook/chapters/graphml.pdf
http://graphml.graphdrawing.org/primer/graphml-primer.html
*/

/*
TODO:
    6. Allow every graph not only tree by finding min. spannig tree first
    7. Fiter_applying multithreading
        
*/

namespace Walrus
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Graph mGraph;
        
        ContainerUIElement3D graphModel = null;
        Model3DGroup edgesModel = null;
        Model3DGroup ringsModel = null;

        private bool mMouseLeftButtonDown;
        private bool mMouseRightButtonDown;
        private Point mMouseLastPos;

        public MainWindow()
        {
            InitializeComponent();
            BuildGeometry();

            Camera.Instance().setCamera(camera);

            //mCamera = new Camera(camera);
        }

        /// <summary>
        /// Create basic components
        /// </summary>
        private void BuildGeometry()
        {
            ringsModel = new Model3DGroup();
            ringsModel.Children.Add(GeometryBuilder.BuildRingX(2, 0.005, 80, new DiffuseMaterial(System.Windows.Media.Brushes.Green)));
            ringsModel.Children.Add(GeometryBuilder.BuildRingY(2, 0.005, 80, new DiffuseMaterial(System.Windows.Media.Brushes.Green)));
            ringsModel.Children.Add(GeometryBuilder.BuildRingZ(2, 0.005, 80, new DiffuseMaterial(System.Windows.Media.Brushes.Green)));

            group.Children.Add(ringsModel);
        }

        /// <summary>
        /// Load graph from file
        /// </summary>
        private void LoadClick(object sender, RoutedEventArgs e)
        {

            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".graphml";
            dlg.Filter = "GRAPHML Files (*.graphml)|*.graphml";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            viewport.Children.Remove(graphModel);
            group.Children.Remove(edgesModel);
            group.Children.Remove(ringsModel);

            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                mGraph = new Graph();
                viewport.Children.Remove(mGraph.getGeometry());

                string filename = dlg.FileName;
                //load graph
                try
                {
                    mGraph.LoadGraph(filename, Info);
                }
                catch (Exception ex)
                {
                    Trace.WriteLine("Graph loading exception: " + ex.ToString());
                    Info.Visibility = Visibility.Visible;
                    Info.Content = "WRONG GRAPH";
                    return;
                }

                //find spanning tree if necessary
                try
                {
                    mGraph.FindSpanningTree();
                }
                catch (Exception ex)
                {
                    Trace.WriteLine("Graph loading exception: " + ex.ToString());
                    Info.Visibility = Visibility.Visible;
                    Info.Content = "SPANNING TREE ERROR";
                    return;
                }

                //set nodes positions in 3d
                try
                {
                    mGraph.Render();
                }
                catch (Exception ex)
                {
                    Trace.WriteLine("Graph rendering exception: " + ex.ToString());
                    Info.Visibility = Visibility.Visible;
                    Info.Content = "Error while rendering";
                    return;
                }
                //get geometry 
                graphModel = mGraph.getGeometry();
                viewport.Children.Add(graphModel);
                edgesModel = mGraph.getEdgesGeometry();
                group.Children.Add(edgesModel);

                //nodes collapsing filter
                mGraph.applyFilter();

                //rings model
                ringsModel = new Model3DGroup();
                ringsModel.Children.Add(GeometryBuilder.BuildRingX(mGraph.R, 0.005, 80, new DiffuseMaterial(System.Windows.Media.Brushes.Green)));
                ringsModel.Children.Add(GeometryBuilder.BuildRingY(mGraph.R, 0.005, 80, new DiffuseMaterial(System.Windows.Media.Brushes.Green)));
                ringsModel.Children.Add(GeometryBuilder.BuildRingZ(mGraph.R, 0.005, 80, new DiffuseMaterial(System.Windows.Media.Brushes.Green)));
                group.Children.Add(ringsModel);

                //dipslay graph info to user
                GraphData.Content = "Nodes: " + mGraph.NodesNr + "   Edges: " + mGraph.EdgesNr;
            }
        }

        /// <summary>
        /// Reset camera postion
        /// </summary>
        private void ResetClick(object sender, RoutedEventArgs e)
        {
            Camera.Instance().Reset();
        }

        /// <summary>
        /// move camera forwar/backward
        /// </summary>
        private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            Camera.Instance().changeR(e.Delta);
            NodeManipulator.Instance().extend(e.Delta);
        }

        /// <summary>
        /// Mouse button down - start camera rotation/movement
        /// </summary>
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && mMouseRightButtonDown == false)
                mMouseLeftButtonDown = true;
            if (e.RightButton == MouseButtonState.Pressed && mMouseLeftButtonDown == false)
                mMouseRightButtonDown = true;

            if(mMouseRightButtonDown || mMouseLeftButtonDown)
                mMouseLastPos = new Point(
                    Mouse.GetPosition(viewport).X, Mouse.GetPosition(viewport).Y);
        }
        
        /// <summary>
        /// Mouse button up - stop camera rotation/movement
        /// </summary>
        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if(e.LeftButton == MouseButtonState.Released)
                mMouseLeftButtonDown = false;

            if (e.RightButton == MouseButtonState.Released)
            {
                Camera.Instance().Unfreeze();
                mMouseRightButtonDown = false;
                NodeManipulator.Instance().EndManipulation();
            }
        }

        /// <summary>
        /// Move/rotate camera
        /// </summary>
        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            if (!(mMouseLeftButtonDown || mMouseRightButtonDown))
                return;

            Point actualPos = new Point(
                Mouse.GetPosition(viewport).X, Mouse.GetPosition(viewport).Y);

            double dx = (actualPos.X - mMouseLastPos.X);
            double dy = -(actualPos.Y - mMouseLastPos.Y);

            if (mMouseLeftButtonDown)
                Camera.Instance().Rotate(dx / 300, dy / 300);
            else
                Camera.Instance().Move(dx, dy);

            NodeManipulator.Instance().move(dx, dy);

            mMouseLastPos = actualPos;
        }

        /// <summary>
        /// Set new filter for collapsing nodes
        /// </summary>
        private void applyFilter_Click(object sender, RoutedEventArgs e)
        {
            string nr = filter.Text;
            //check if is proper number
            Regex regex = new Regex("[^0-9]+");
            if (regex.IsMatch(nr))
                return;

            mGraph.Filter = Int32.Parse(nr);
            mGraph.applyFilter();
            Trace.WriteLine("Filter applied: " + nr);
        }
    }
}
